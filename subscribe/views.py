import colander
import deform.widget

from pyramid.httpexceptions import HTTPFound
from pyramid.view import (
    view_config,
    view_defaults,
    forbidden_view_config
    )

from pyramid.security import (
	remember,
	forget,
	)

from .security import (
    USERS,
    check_password
)


from .models import DBSession, Page


class TaskPage(colander.MappingSchema):
    Subscriber = colander.SchemaNode(colander.String())
    Email = colander.SchemaNode(colander.String(),
                                validator=colander.Email()
                                )

@view_defaults(renderer='home.pt')
class TaskViews(object):
    def __init__(self, request):
        self.request = request
        self.logged_in = request.authenticated_userid

    @property
    def task_form(self):
        schema = TaskPage()
        return deform.Form(schema, buttons=('subscribe',))

    @property
    def reqts(self):
        return self.task_form.get_widget_resources()

    @view_config(route_name='home')
    def home(self):
        return {'name': 'Home page'}

    @view_config(route_name='task_view', renderer='task_view.pt', permission='edit')
    def task_view(self):
        pages = DBSession.query(Page).order_by(Page.subscriber)
        return dict(title='Task View', pages=pages)

    @view_config(route_name='taskpage_add',
                 renderer='taskpage_addedit.pt')
    def taskpage_add(self):
        request = self.request
        headers = forget(request)
        form = self.task_form.render()
        if 'subscribe' in self.request.params:
            controls = self.request.POST.items()
            try:
                appstruct = self.task_form.validate(controls)
            except deform.ValidationFailure as e:
                # Form is NOT valid
                return dict(form=e.render())

            # Add a new page to the database
            new_subscriber = appstruct['Subscriber']
            new_email = appstruct['Email']
            DBSession.add(Page(subscriber=new_subscriber, email=new_email))

            # Get the new ID and redirect
            page = DBSession.query(Page).filter_by(subscriber=new_subscriber).one()
            new_uid = page.uid

            url = self.request.route_url('taskpage_view', uid=new_uid)
            return HTTPFound(location=url,
                         headers=headers)
        return dict(form=form)


    @view_config(route_name='taskpage_view', renderer='taskpage_view.pt')
    def taskpage_view(self):
        request = self.request
        headers = forget(request)
        uid = int(self.request.matchdict['uid'])
        page = DBSession.query(Page).filter_by(uid=uid).one()
        return dict(page=page, headers=headers)

    @view_config(route_name='taskpage_edit',
                 renderer='taskpage_addedit.pt')
    def taskpage_edit(self):
        uid = int(self.request.matchdict['uid'])
        page = DBSession.query(Page).filter_by(uid=uid).one()

        task_form = self.task_form

        if 'subscribe' in self.request.params:
            controls = self.request.POST.items()
            try:
                appstruct = task_form.validate(controls)
            except deform.ValidationFailure as e:
                return dict(page=page, form=e.render())

            # Change the content and redirect to the view
            page.subscriber = appstruct['Subscriber']
            page.email = appstruct['Email']
            url = self.request.route_url('taskpage_view', uid=uid)
            return HTTPFound(url)

        form = self.task_form.render(dict(
            uid=page.uid, subscriber=page.subscriber, email=page.email)
        )

        return dict(page=page, form=form)

    @view_config(route_name='taskpage_delete', renderer='taskpage_view.pt')
    def taskpage_delete(self):
        request = self.request
        uid = int(self.request.matchdict['uid'])
        page = DBSession.query(Page).filter_by(uid=uid).one()
        DBSession.delete(page)
        url = request.route_url('task_view')
        return HTTPFound(location=url)

    @view_config(route_name='login', renderer='login.pt')
    @forbidden_view_config(renderer='login.pt')
    def login(self):
        request = self.request
        login_url = request.route_url('login')
        referrer = request.url
        if referrer == login_url:
            referrer = '/'  # never use login form itself as came_from
        came_from = request.params.get('came_from', referrer)
        message = ''
        login = ''
        password = ''
        if 'form_login.submitted' in request.params:
            login = request.params['login']
            password = request.params['password']
            if check_password(password, USERS.get(login)):
                headers = remember(request, login)
                return HTTPFound(location=came_from,
                                 headers=headers)
            message = 'Failed login'

        return dict(
            name='Login',
            message=message,
            url_login=request.application_url + '/view',
            came_from=came_from,
            login=login,
            password=password,
        )


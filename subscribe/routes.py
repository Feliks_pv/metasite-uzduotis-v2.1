def includeme(config):
    config.add_route('home', '/')
    config.add_route('task_view', '/view')
    config.add_route('taskpage_add', '/add')
    config.add_route('taskpage_view', '/{uid}')
    config.add_route('taskpage_edit', '/{uid}/edit')
    config.add_route('taskpage_delete', '/{uid}/delete')
    config.add_route('login', '/login')
    config.add_static_view('deform_static', 'deform:static/')


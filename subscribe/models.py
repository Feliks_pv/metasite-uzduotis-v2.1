from pyramid.security import Allow, Everyone

from sqlalchemy import (
    Column,
    Integer,
    Text,
    )

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    )

from zope.sqlalchemy import ZopeTransactionExtension

DBSession = scoped_session(
    sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()


class Page(Base):
    __tablename__ = 'taskpages'
    uid = Column(Integer, primary_key=True)
    subscriber = Column(Text, unique=True)
    email = Column(Text)

class Root(object):
    __acl__ = [(Allow, Everyone, 'view'),
               (Allow, 'group:masters', 'edit')]

    def __init__(self, request):
        pass


from setuptools import setup

requires = [
    'pyramid',
    'pyramid_chameleon',
    'bcrypt',
    'deform',
    'sqlalchemy',
    'pyramid_tm',
    'zope.sqlalchemy'
]

setup(name='subscribe',
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = subscribe:main
      [console_scripts]
      initialize_subscribe_db = subscribe.initialize_db:main
      """,
)


